/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import {throttle} from './libs/helpers';
import {setupCards} from './components/setup-cards';

const THROTTLE_DELAY = 500;
const SCROLL_OFFSET = 250;

const handleScroll = throttle(() => {
    const cardsWrapper = document.querySelector('.cards');
    if (!(cardsWrapper.classList.contains('loading'))) {
        if (window.innerHeight + window.scrollY + SCROLL_OFFSET >= cardsWrapper.offsetHeight) {
            setupCards();
        }
    }
}, THROTTLE_DELAY);

window.addEventListener('scroll', function () {
    handleScroll();
});
window.addEventListener('DOMContentLoaded', function pageLoad() {
    window.removeEventListener('DOMContentLoaded', pageLoad);
    setupCards();
});

// this tells webpack this file is available for HMR
if (module.hot) {
    module.hot.accept();
}
