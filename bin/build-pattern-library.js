const fse = require('fs-extra');
const path = require('path');

function dirTree(filename) {
    return new Promise((resolve, reject) => {
        const patternLibrary = {
            patternLibraryNav: []
        };
        const info = fse.readdirSync(filename).map(file => ({
            link: `/pattern-library/${path.basename(file).replace('.njk', '.html')}`,
            name: path.basename(file).replace('.njk', '')
        }));
        patternLibrary.patternLibraryNav = info;
        fse.writeJson(
            './src/templates/data/pattern-library.json',
            patternLibrary,
            {
                spaces: 4
            },
            function (err) {
                if (err) {
                    reject(err);
                }
                resolve();
            }
        );
    });
}

module.exports = dirTree;
