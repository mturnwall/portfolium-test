// Requires https://www.npmjs.com/package/@fdaciuk/ajax
import ajax from '@fdaciuk/ajax';

/**
 *  Object that holds all the API endpoints.
 *  @example
 *  {
 *      apiName: '/apiPath'
 *  }
 */
const endpoints = {
    expert: '/expert',
};

/**
 *  The URL (host) of your API
 *  @example https://private-0fde15-pto.apiary-mock.com/
 */
const API_HOST = 'https://api.portfolium.com';
/**
 *  The path to the API, this is the part that comes after the host
 *  @example https://private-0fde15-pto.apiary-mock.com/API_PATH
 */
const API_PATH = '/entries';

/**
 *
 * @param {string} endpoint - api endpoint to call
 * @param {*} data - data that will be sent to the server
 * @param {string} [method=POST] - the http action such as GET or POST
 * @returns {Promise} - Promise wrapper for the ajax call
 */
const api = (endpoint, data, method = 'POST') => new Promise((resolve, reject) => {
    if (!(endpoint in endpoints)) {
        reject(new Error(`The api endpoint "${endpoint}" was not found`));
        return false;
    }
    ajax({
        method,
        data,
        url: `${API_HOST}${API_PATH}${endpoints[endpoint]}`,
    }).then(response => {
        resolve(response);
    }).catch(err => {
        reject(err);
    });
    return true;
});

export {
    api
};
