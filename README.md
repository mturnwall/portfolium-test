# Portfolium test

## Setup

Clone the repository to your local machine. In the root folder run `npm install`. This will download all the dependencies.

## Using

To run the project locally, use the command `npm run serve`. This will build the files in memory for development and start a local server at `http://localhost:3001`

When you are ready to build, just run `npm run build`. This will build the project into the `dist` folder.

If you need to change your supported browsers you can do so by modifying the `browserslist` file. Here is [documentation](https://github.com/browserslist/browserslist) for the browserlist file 
