# Module folder

This folder holds the sass files for each module. Each module should 
have it's own module. The naming convention should be the module name 
prefixed with `_module`. For example an article module would be 
`_module_article.scss`.
