import {getCards} from './get-cards';
import {createCard} from './create-card';

let cardsOffset = 0;
const CARDS_LIMIT = 12;

const setupCards = () => {
    const cardsWrapper = document.querySelector('.cards');
    cardsWrapper.classList.add('loading');
    const docFrag = document.createDocumentFragment();
    const gettingCards = getCards(cardsOffset, CARDS_LIMIT);
    cardsOffset += CARDS_LIMIT;
    gettingCards.then(data => {
        data.forEach(card => {
            const cardHtml = createCard(card);
            docFrag.appendChild(cardHtml);
        });
        cardsWrapper.appendChild(docFrag);
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        cardsWrapper.classList.remove('loading');
    });
};

export {
    setupCards,
};
