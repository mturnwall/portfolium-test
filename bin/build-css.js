const fs = require('fs');
const mkdirp = require('mkdirp');
const sass = require('node-sass');
const importer = require('node-sass-magic-importer');
const postcss = require('postcss');
const autoprefixer = require('autoprefixer');
const run = require('./run');
const chokidar = require('chokidar');
const log = require('./log');
const pkg = require('../package.json');

const cssDir = process.env.npm_package_config_cssOut;
const sassFilename = process.env.npm_package_config_cssMain;
const cssFiles = pkg.config.cssFiles;

function compileSass(file) {
    return sass.renderSync({
        file: `./src/sass/${file}.scss`,
        outFile: `${cssDir}/${file}.css`,
        outputStyle: 'expanded',
        sourceMap: true,
        sourceMapContents: true,
        importer: importer()
    });
}

function processCss(css, file) {
    const processor = postcss([autoprefixer]);
    processor.
        process(css.css, {
            from: `./src/sass/${file}.scss`,
            to: `${cssDir}/${file}.css`,
            map: {
                prev: css.map.toString(),
                inline: false
            }
        }).
        then(postResult => {
            fs.writeFileSync(`${cssDir}/${file}.css`, postResult.css, err => {
                if (err) {
                    console.log('writeFile error:', err);
                }
            });
            if (postResult.map) {
                fs.writeFileSync(
                    `${cssDir}/${file}.css.map`,
                    postResult.map,
                    err => {
                        if (err) {
                            console.log('writeFile error:', err);
                        }
                    }
                );
            }
        });
}

function watchCss() {
    log('notice', 'Start watching sass files...');
    const watcher = chokidar.watch('src/sass');
    watcher.on('change', async filepath => {
        log('success', `${filepath} was updated.`);
        await run(buildCss);
    });
}

function buildCss(options) {
    const watch = options.includes('--watch');
    if (!fs.existsSync(cssDir)) {
        mkdirp.sync(cssDir, err => {
            if (err) {
                console.log(err);
            }
        });
    }

    const promiseArray = cssFiles.map(
        file => new Promise((resolve, reject) => {
            const result = compileSass(file);
            processCss(result, file);
            if (watch) {
                watchCss();
            }
            resolve();
        })
    );

    return Promise.all([promiseArray]);
}

module.exports = {
    default: buildCss
};
