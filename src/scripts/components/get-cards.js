import {api} from './api';

const getCards = (start, limit) => new Promise((resolve, reject) => {
    if (typeof start === 'undefined') {
        reject(new Error('You need to provide a start index'));
    }
    if (typeof limit === 'undefined') {
        reject(new Error('You need to provide the limit argument'));
    }
    api('expert', `offset=${start}&limit=${limit}&sort=recent&X-API-KEY=devtest`, 'GET')
        .then(data => {
            resolve(data);
        }).catch(err => {
            reject(err);
        });
});

export {
    getCards,
};
