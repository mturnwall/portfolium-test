import {wire} from 'hyperhtml';

const CDN = 'https://cdn.portfolium.com/';
const IMAGE_SERVICE = 'https://portfolium1.cloudimg.io/s/crop/390x219/';
const DEFAULT_IMAGE = 'img/defaults/default.png';

const createCard = props => {
    const source = encodeURI(props.media[0].filename.source);
    const cardPath = (/\.(png|gif|jpg|jpeg|tiff)$/.test(source)) ? source : DEFAULT_IMAGE;
    /*
     * build image src here to avoid a nested template literal
     * in the wire() template
     */
    const cardImage = `${IMAGE_SERVICE}${CDN}${cardPath}`;
    return wire()`
        <section class="card">
            <img class="card__image" src="${cardImage}" alt="">
            <h1 class="card__title">${props.title}</h1>
            <p class="card__description">${props.description}</p>
        </section>`;
};

export {
    createCard,
};
