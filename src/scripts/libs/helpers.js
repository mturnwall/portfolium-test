
const throttle = (func, delay) => {
    let throttled = false;
    const later = (...args) => {
        if (throttled) {
            return;
        }
        throttled = true;
        func.apply(this, args);
        setTimeout(() => {
            throttled = false;
            later.apply(this, args);
        }, delay);
    };
    return later;
};

export {
    throttle,
};
